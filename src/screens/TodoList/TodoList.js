import React, { useState }  from 'react';
import {
    Text,
    View,
    StyleSheet,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    Image,
    Modal,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import CheckBox from '@react-native-community/checkbox'; 

export function TodoList(props) {
    const [data, setData] = useState([
        {
            id: 1,
            title: 'Task 1',
            active: false
        }
    ]);
    const [isOpen, setIsOpen] = useState(false);
    const [title, setTitle] = useState('');

    const renderItem = ({item, index}) => {
        return (
            <View style={styles.todoItem}>
                <CheckBox
                    disabled={false}
                    value={item.active}
                    onValueChange={newValue => setToggleCheckBox(newValue, index)}
                />
                <Text
                    style={{ textDecorationLine: item.active ? 'line-through' : 'none' }}
                >{item.title}</Text>
            </View>
        );
    };

    const storeData = () => {
        let newArr = [...data];
        newArr.push({ id: newArr.length + 1, title: title, active: false });
        setData(newArr);
    }

    const setToggleCheckBox = (value, index) => {
        let newArr = [...data];

        newArr[index].active = !newArr[index].active;

        setData(newArr);
    }

    return (
        <View style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
                <Text style={styles.title}>Todos</Text>
                <FlatList data={data} renderItem={renderItem} />
            </SafeAreaView>
            <TouchableOpacity style={styles.addBtnWrapper} onPress={() => setIsOpen(true)}>
                <Image
                    style={styles.addIcon}
                    source={ require('../../../assets/img/add-icon.png') }
                />
            </TouchableOpacity>

            <Modal transparent={true} visible={isOpen}>
                <View style={styles.modalContentContainer}>
                    <TouchableOpacity
                        style={styles.closeBtnWrapper}
                        onPress={() => setIsOpen(false)}
                    >
                        <Icon
                            color="white"
                            name="closecircle"
                            size={20}
                        />
                        {/* <Image
                            style={styles.closeIcon}
                            source={ require('../../../assets/img/close.png') }
                        /> */}
                    </TouchableOpacity>

                    <View style={styles.formWrapper}>
                        <View style={styles.inputWrapper}>
                            <TextInput
                                style={styles.textInput}
                                placeholder={'Please enter the task title'}
                                onChangeText={ text => setTitle(text) }
                            />
                        </View>
                        <TouchableOpacity
                            style={styles.btnWrapper}
                            onPress={() => storeData()}
                        >
                            <Text>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 10
    },
    contentContainer: {
        display: 'flex',
        flex: 1
    },
    todoItem: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    addBtnWrapper: {
        alignItems: 'center',
        paddingBottom: 10
    },
    addIcon: {
        width: 50,
        height: 50
    },
    modalContentContainer: {
        height: '50%',
        marginTop: 'auto',
        backgroundColor: 'green',
        padding: 15
    },
    closeBtnWrapper: {
        alignItems: 'flex-end'
    },
    closeIcon: {
        width: 20,
        height: 20,
    },
    formWrapper: {
        marginTop: 20
    },
    inputWrapper: {
        borderRadius: 50,
        backgroundColor: 'white',
        padding: 10,
    },
    textInput: {
        paddingLeft: 10,
        backgroundColor: 'white',
        fontSize: 15,
    },
    btnWrapper: {
        borderRadius: 50,
        backgroundColor: 'orange',
        marginTop: 20,
        padding: 15,
        alignItems: 'center',
        fontSize: 15,
        fontWeight: 'bold'
    }
});
