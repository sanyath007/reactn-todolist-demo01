import React from 'react';
import { StatusBar } from 'expo-status-bar';
import {
  StyleSheet,
  SafeAreaView,
  Platform
} from 'react-native';
import { TodoList } from './src/screens/TodoList';

export default function App() {  
  return (
    <SafeAreaView style={styles.container}>
      <TodoList />

      <StatusBar />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: { 
    display: 'flex',
    flex: 1,
    justifyContent: "center",
    alignItems: 'center',
    backgroundColor: 'gold',
    paddingTop: Platform.OS === 'android' ? 40 : 0
  }
});